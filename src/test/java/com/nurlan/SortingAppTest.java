package com.nurlan;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingAppTest {

    private final int[] actual;
    private final int[] expected;

    SortingApp sortingApp = new SortingApp();

    public SortingAppTest(int[] actual, int[] expected) {
        this.actual = actual;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{100, 99, 88, 77, 66, 55, 44, 33, 22, 11}, new int[]{11, 22, 33, 44, 55, 66, 77, 88, 99, 100}},
                {new int[]{3, 2, 7, 4, 11, 3, -1, -7, 5, 32}, new int[]{-7, -1, 2, 3, 3, 4, 5, 7, 11, 32}},
        });
    }

    @Test
    public void testZeroArgumentCase(){
        int[] array = new int[]{};
        assertArrayEquals(new int[]{}, sortingApp.sort(array));
    }

    @Test
    public void testOneArgumentCase() {
        int[] array = new int[]{1};
        assertArrayEquals( new int[]{1}, sortingApp.sort(array));
    }

    @Test
    public void testTenArgumentCase() {
        assertArrayEquals(sortingApp.sort(actual), expected);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testMoreThanTenArgumentCase() {
        int[] array = new int[]{3, 2, 7, 4, 11, 3, -1, -7, 5, 32, 44};
        sortingApp.sort(array);
    }
}