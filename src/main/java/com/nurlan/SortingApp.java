package com.nurlan;

/**
 * This is a simple Sorting App that takes integers as command-line arguments,
 * sorts them in ascending order, and prints the result.
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class SortingApp {

    private static final Logger logger = LogManager.getLogger(SortingApp.class);

    public int[] sort(int[] array) {
        if (array.length > 10) {
            throw new IllegalArgumentException("The number of arguments is more than 10.");
        }
        Arrays.sort(array);
        return array;
    }

    public static void main(String[] args) {
        logger.info("Sorting App started.");

        if (args.length == 0) {
            logger.warn("No arguments provided. Exiting.");
            System.out.println("No arguments provided.");
            return;
        } else if (args.length > 10) {
            logger.warn("Exceeded number of arguments. Exiting");
            System.out.println("Arguments number should not be more than 10.");
        }

        // Your sorting logic

        logger.info("Sorting App completed.");
    }
}
